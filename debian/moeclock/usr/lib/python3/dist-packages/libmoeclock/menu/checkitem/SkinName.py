
from libmoeclock.menu.checkitem.CheckItem import NagatoCheckItem


class NagatoSkinName(NagatoCheckItem):

    RADIO = True

    def _on_activate(self, *args):
        yuki_data = "clock", "skin", self._query
        self._raise("YUKI.N > config", yuki_data)
        self._raise("YUKI.N > queue draw")

    def _on_map(self, menu_item):
        yuki_data = "clock", "skin"
        yuki_position = self._enquiry("YUKI.N > config", yuki_data)
        self.handler_block(self._signal_id)
        self.set_active(yuki_position == self._query)
        self.handler_unblock(self._signal_id)

    def _on_initialize(self, user_data):
        yuki_label, yuki_query = user_data
        self.set_label(yuki_label)
        self._query = yuki_query

