
from libmoeclock.menu.sub.Sub import NagatoSub
from libmoeclock.menu.checkitem.BalloonPosition import NagatoBalloonPosition

POSITIONS = {"top left":3, "top right":1, "bottom left":2, "bottom right": 0}


class NagatoBalloonConfig(NagatoSub):

    TITLE = "Balloon"

    def _on_map(self, widget):
        self.show_all()

    def _initialize_child_menus(self):
        for yuki_key, yuki_value in POSITIONS.items():
            NagatoBalloonPosition(self, (yuki_key, yuki_value))
