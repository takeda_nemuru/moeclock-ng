
from libmoeclock.menu.sub.Sub import NagatoSub
from libmoeclock.menu.checkitem.SkinName import NagatoSkinName

SKINS = {
    "Default": "default",
    "Mikunchu": "mikunchu",
    "Blue": "moeskin_blue",
    "Blue Slim": "moeskin_blue_slim",
    "Blue Green": "moeskin_bluegreen",
    "Blue Green Slim": "moeskin_bluegreen_slim",
    "Green": "moeskin_green",
    "Green Slim": "moeskin_green_slim",
    "Heart": "moeskin_heart",
    "Heart 2": "moeskin_heart02"
    }


class NagatoSkinConfig(NagatoSub):

    TITLE = "Skin"

    def _initialize_child_menus(self):
        for yuki_key, yuki_value in SKINS.items():
            NagatoSkinName(self, (yuki_key, yuki_value))
