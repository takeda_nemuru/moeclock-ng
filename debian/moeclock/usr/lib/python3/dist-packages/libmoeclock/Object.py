

class NagatoObject(object):

    def _decode_message(self, message, header):
        yuki_message = message.replace("YUKI.N > ", header)
        return yuki_message.replace(" ", "_")

    def _on_method_found(self, message, user_data=None):
        yuki_method = getattr(self, message)
        return yuki_method() if user_data is None else yuki_method(user_data)

    def _enquiry(self, message, user_data=None):
        yuki_message = self._decode_message(message, "_inform_")
        return self._request_synchronization(yuki_message, user_data)

    def _raise(self, message, user_data=None):
        yuki_message = self._decode_message(message, "_yuki_n_")
        return self._request_synchronization(yuki_message, user_data)

    def _request_synchronization(self, message, user_data=None):
        if self._parent is not None:
            return self._parent.synchronize_nagato_object(message, user_data)

    def synchronize_nagato_object(self, message, user_data=None):
        if message in dir(self):
            return self._on_method_found(message, user_data)
        else:
            return self._request_synchronization(message, user_data)
