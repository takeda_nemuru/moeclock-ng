
from gi.repository import GLib
from libmoeclock.Object import NagatoObject
from libmoeclock.ui.MainWindow2 import NagatoMainWindow2
from libmoeclock.util.ImagePaths import NagatoImagePaths


class NagatoImagePathsLayer(NagatoObject):

    def _yuki_n_change_image(self):
        self._index = GLib.random_int_range(0, self._image_paths.length)

    def _inform_image_path(self):
        return self._image_paths[self._index]

    def _yuki_n_reload_image_paths(self):
        self._image_paths.reload()
        self._index = GLib.random_int_range(0, self._image_paths.length)

    def __init__(self, parent):
        self._parent = parent
        self._image_paths = NagatoImagePaths(self)
        self._index = GLib.random_int_range(0, self._image_paths.length)
        NagatoMainWindow2(self)
