
from gi.repository import GLib
from libmoeclock.Object import NagatoObject
from libmoeclock.dataovermind.ResourcesLayer import NagatoResourcesLayer
from libmoeclock.util.UnixSignal import HaruhiUnixSignal


class NagatoYuki(NagatoObject):

    def _inform_library_directory(self):
        return GLib.path_get_dirname(__file__)

    def N(self, message):
        print("Welcome back, {}.".format(GLib.get_real_name()))
        HaruhiUnixSignal()
        NagatoResourcesLayer(self)
        print("See you.")

    def __init__(self):
        self._parent = None
