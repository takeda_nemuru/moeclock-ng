
from libmoeclock.Object import NagatoObject
from libmoeclock.util.RecursivePathLoader import NagatoRecursivePathLoader


class NagatoImagePaths(NagatoObject):

    def _yuki_n_path_loaded(self, path):
        self._paths.append(path)

    def reload(self):
        self._paths.clear()
        yuki_data = "clock", "wallpaper_directory"
        self._directory = self._enquiry("YUKI.N > config", yuki_data)
        self._loader.load(self._directory, "image")

    def __getitem__(self, index):
        return self._paths[index]

    def __init__(self, parent):
        self._parent = parent
        self._paths = []
        self._loader = NagatoRecursivePathLoader(self)
        self.reload()

    @property
    def length(self):
        return len(self._paths)
