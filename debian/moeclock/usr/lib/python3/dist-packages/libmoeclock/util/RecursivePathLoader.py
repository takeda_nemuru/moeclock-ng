
from gi.repository import Gio
from gi.repository import GLib
from libmoeclock.Object import NagatoObject


class NagatoRecursivePathLoader(NagatoObject):

    def _analyze_content(self, directory, file_info):
        yuki_path = GLib.build_filenamev([directory, file_info.get_name()])
        yuki_content_type = file_info.get_content_type()
        if yuki_content_type == "inode/directory":
            self._parse_directory(yuki_path)
        if yuki_content_type.startswith(self._content_type):
            self._raise("YUKI.N > path loaded", yuki_path)

    def _parse_directory(self, directory):
        yuki_gio_file = Gio.File.new_for_path(directory)
        for yuki_file_info in yuki_gio_file.enumerate_children("*", 0, None):
            self._analyze_content(directory, yuki_file_info)

    def load(self, directory, content_type):
        self._content_type = content_type
        self._parse_directory(directory)

    def __init__(self, parent):
        self._parent = parent
        self._content_type = ""
