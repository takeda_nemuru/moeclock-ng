
from gettext import gettext as _
from gi.repository import Gtk
from gi.repository import GdkPixbuf
from libmoeclock.Object import NagatoObject

VERSION = "42.17.4"
COMMENTS = _("Change the image every minute and notify the time.")


class NagatoAboutDialog(NagatoObject):

    def _on_response(self, dialog, response):
        dialog.destroy()

    def run_dialog(self, menu_item):
        yuki_path = self._enquiry("YUKI.N > resource", "application.png")
        yuki_dialog = Gtk.AboutDialog()
        yuki_dialog.set_program_name(_("Moe Clock"))
        yuki_dialog.set_version(VERSION)
        yuki_dialog.set_license("MIT")
        yuki_dialog.set_comments(COMMENTS)
        yuki_dialog.set_authors([_("Kaorin@")])
        yuki_dialog.set_documenters([_("Kaorin@")])
        yuki_dialog.set_logo(GdkPixbuf.Pixbuf.new_from_file(yuki_path))
        yuki_dialog.connect("response", self._on_response)
        yuki_dialog.show()

    def __init__(self, parent):
        self._parent = parent
