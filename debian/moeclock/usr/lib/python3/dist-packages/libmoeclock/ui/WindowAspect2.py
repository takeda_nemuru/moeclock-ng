
from gi.repository import Gdk
from libmoeclock.Object import NagatoObject
from libmoeclock.ui.DrawingArea import NagatoDrawingArea

WINDOW_HINTS = Gdk.WindowHints.ASPECT


class NagatoWindowAspect(NagatoObject):

    def _set_geometry(self, size):
        yuki_width, yuki_height = size
        yuki_geometry = Gdk.Geometry()
        yuki_geometry.max_aspect = yuki_width/yuki_height
        yuki_geometry.min_aspect = yuki_width/yuki_height
        self._gdk_window.set_geometry_hints(yuki_geometry, WINDOW_HINTS)

    def _yuki_n_resize(self, size):
        self._set_geometry(size)
        yuki_x, yuki_y = self._gdk_window.get_root_origin()
        self._gdk_window.move(yuki_x, yuki_y)
        self._gdk_window.resize(*size)

    def _on_realize(self, window):
        self._gdk_window = window.get_window()

    def __init__(self, parent):
        self._parent = parent
        yuki_window = self._enquiry("YUKI.N > main window")
        yuki_window.connect("realize", self._on_realize)
        NagatoDrawingArea(self)
