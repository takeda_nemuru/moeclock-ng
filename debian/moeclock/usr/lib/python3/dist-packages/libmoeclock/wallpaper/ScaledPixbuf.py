
from gi.repository import GdkPixbuf
from libmoeclock.Object import NagatoObject


class NagatoScaledPixbuf(NagatoObject):

    def build_(self):
        yuki_path = self._enquiry("YUKI.N > image path")
        yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_file(yuki_path)
        yuki_aspect = yuki_pixbuf.get_height()/yuki_pixbuf.get_width()
        yuki_width, __ = self._enquiry("YUKI.N > window size")
        yuki_height = yuki_width*yuki_aspect
        self._raise("YUKI.N > resize", (yuki_width, yuki_height))
        yuki_data = yuki_width, yuki_height, GdkPixbuf.InterpType.BILINEAR
        yuki_scaled_pixbuf = yuki_pixbuf.scale_simple(*yuki_data)
        return yuki_scaled_pixbuf.copy()

    def __init__(self, parent):
        self._parent = parent
