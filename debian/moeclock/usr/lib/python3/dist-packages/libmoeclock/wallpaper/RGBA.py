
from gi.repository import Gdk
from gi.repository import GLib
from libmoeclock.Object import NagatoObject


class NagatoRGBA(NagatoObject):

    def _get_theme_color(self):
        yuki_skin = self._enquiry("YUKI.N > config", ("clock", "skin"))
        yuki_directory = self._enquiry("YUKI.N > resource", "skin")
        yuki_names = [yuki_directory, yuki_skin, "color_setting.txt"]
        yuki_color_setting_path = GLib.build_filenamev(yuki_names)
        yuki_, yuki_bytes = GLib.file_get_contents(yuki_color_setting_path)
        return yuki_bytes.decode("utf-8").split()[0]

    def get_rgba(self):
        yuki_rgba = Gdk.RGBA()
        yuki_rgba.parse(self._get_theme_color())
        return yuki_rgba.red, yuki_rgba.green, yuki_rgba.blue

    def __init__(self, parent):
        self._parent = parent
