
from gi.repository import Gdk
from gi.repository import GLib
from gi.repository import GdkPixbuf
from libmoeclock.Object import NagatoObject
from libmoeclock.wallpaper.Balloon import NagatoBalloon
from libmoeclock.wallpaper.Chrome import NagatoChrome


class NagatoFrame(NagatoObject):

    def paint(self, cairo_context):
        yuki_frame_pixbuf = self._chrome.get_scaled()
        self._balloon.composit(yuki_frame_pixbuf)
        yuki_config_data = "clock", "annotation_type"
        yuki_position = self._enquiry("YUKI.N > config", yuki_config_data)
        if yuki_position in [2, 3]:
            yuki_frame_pixbuf = yuki_frame_pixbuf.flip(True)
        if yuki_position in [1, 3]:
            yuki_frame_pixbuf = yuki_frame_pixbuf.flip(False)
        yuki_data = cairo_context, yuki_frame_pixbuf, 0, 0
        Gdk.cairo_set_source_pixbuf(*yuki_data)
        cairo_context.paint_with_alpha(0.8)

    def __init__(self, parent):
        self._parent = parent
        self._balloon = NagatoBalloon(self)
        self._chrome = NagatoChrome(self)
