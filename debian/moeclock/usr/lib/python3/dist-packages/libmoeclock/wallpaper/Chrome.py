
from gi.repository import GdkPixbuf
from libmoeclock.wallpaper.Themed import NagatoThemed


class NagatoChrome(NagatoThemed):

    def get_scaled(self):
        yuki_pixbuf = self._get_pixbuf_from_name("frame.png")
        yuki_size = self._enquiry("YUKI.N > window size")
        yuki_interp_type = GdkPixbuf.InterpType.BILINEAR
        yuki_pixbuf = yuki_pixbuf.scale_simple(*yuki_size, yuki_interp_type)
        return yuki_pixbuf.copy()
