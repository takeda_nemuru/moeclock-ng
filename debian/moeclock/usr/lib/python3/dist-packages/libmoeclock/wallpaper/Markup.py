
from gi.repository import Pango
from gi.repository import PangoCairo
from libmoeclock.Object import NagatoObject
from libmoeclock.wallpaper import FontDescription
from libmoeclock.wallpaper import DateTime
from libmoeclock.wallpaper.RGBA import NagatoRGBA


class NagatoMarkup(NagatoObject):

    def _get_y(self, drawable_size, layout_size, position):
        return drawable_size-layout_size-8 if position in [0, 2] else 8

    def _get_positions(self, layout_size):
        yuki_width, yuki_height = self._enquiry("YUKI.N > window size")
        yuki_config_data = "clock", "annotation_type"
        yuki_position = self._enquiry("YUKI.N > config", yuki_config_data)
        yuki_x = yuki_width-120 if yuki_position in [0, 1] else 0
        yuki_y = self._get_y(yuki_height, layout_size.height, yuki_position)
        return yuki_x, yuki_y

    def paint(self, cairo_context):
        yuki_layout = PangoCairo.create_layout(cairo_context)
        yuki_layout.set_markup(DateTime.get_markup())
        yuki_layout.set_font_description(FontDescription.get())
        yuki_layout.set_width(120*Pango.SCALE)  # really ?
        yuki_layout.set_alignment(Pango.Alignment.CENTER)
        yuki_positions = self._get_positions(yuki_layout.get_pixel_size())
        cairo_context.set_source_rgba(*self._rgba.get_rgba())
        cairo_context.move_to(*yuki_positions)
        PangoCairo.update_layout(cairo_context, yuki_layout)
        PangoCairo.show_layout(cairo_context, yuki_layout)

    def __init__(self, parent):
        self._parent = parent
        self._rgba = NagatoRGBA(self)
