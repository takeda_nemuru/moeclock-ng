
from gi.repository import Gtk
from gi.repository import Pango


def get(size=0, weight=400):
    yuki_settings = Gtk.Settings.get_default()
    yuki_font = yuki_settings.get_property("gtk-font-name")
    yuki_font_description = Pango.font_description_from_string(yuki_font)
    yuki_size = yuki_font_description.get_size()/Pango.SCALE
    yuki_font_description.set_size((yuki_size+size)*Pango.SCALE)
    yuki_font_description.set_weight(weight)
    return yuki_font_description
