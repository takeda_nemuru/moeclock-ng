
import gi
import gettext
import locale

gi.require_version('Gtk', '3.0')
gi.require_version('GdkPixbuf', '2.0')
gi.require_version('GstPlayer', '1.0')
gi.require_version('PangoCairo', '1.0')

locale.setlocale(locale.LC_ALL, None)
yuki_names = ('gettext', 'ngettext')
gettext.install("moeclock", "/usr/share/locale", names=yuki_names)
