
from gi.repository import Gtk
from gi.repository import Gdk
from libmoeclock.Object import NagatoObject
from libmoeclock.menu.Item import NagatoItem
from libmoeclock.menu.sub.SkinConfig import NagatoSkinConfig
from libmoeclock.menu.sub.BalloonConfig import NagatoBalloonConfig
from libmoeclock.menu.action.SelectDirectory import NagatoSelectDirectory


class NagatoContext(Gtk.Menu, NagatoObject):

    def _on_button_press(self, widget, event):
        if event.button == 1:
            self._raise("YUKI.N > change image")
        if event.button == 3:
            self.show_all()
            self.popup_at_pointer(None)

    def _initialize_children(self):
        NagatoSelectDirectory(self)
        NagatoSkinConfig(self)
        NagatoBalloonConfig(self)
        NagatoItem(self, "About", "YUKI.N > about")
        NagatoItem(self, "Quit", "YUKI.N > quit")

    def __init__(self, parent):
        self._parent = parent
        self._parent.add_events(Gdk.EventMask.BUTTON_PRESS_MASK)
        self._parent.connect("button-press-event", self._on_button_press)
        Gtk.Menu.__init__(self)
        self._initialize_children()
