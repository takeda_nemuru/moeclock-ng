
from gi.repository import Gtk
from libmoeclock.menu.action.Action import NagatoAction
from libmoeclock.dialog.SelectDirectory import NagatoSelectDirectory as Dialog


class NagatoSelectDirectory(NagatoAction):

    TITLE = "Select Directory"

    def _on_activate(self, menu_item):
        yuki_response = Dialog.call()
        if yuki_response is not None:
            yuki_data = "clock", "wallpaper_directory", yuki_response
            self._raise("YUKI.N > config", yuki_data)
            self._raise("YUKI.N > reload image paths")
