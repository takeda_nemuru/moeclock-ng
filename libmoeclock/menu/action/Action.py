
from gi.repository import Gtk
from libmoeclock.Object import NagatoObject


class NagatoAction(Gtk.MenuItem, NagatoObject):

    TITLE = "title"
    MESSAGE = "YUKI.N > ignore"

    def _on_activate(self, menu_item):
        self._raise(self.MESSAGE)

    def _on_map(self, widget):
        pass

    def _connect_gtk_callbacks(self):
        self.connect("activate", self._on_activate)
        self.connect("map", self._on_map)

    def __init__(self, parent):
        self._parent = parent
        Gtk.MenuItem.__init__(self, self.TITLE)
        self._connect_gtk_callbacks()
        self._parent.append(self)
