
from gi.repository import Gtk
# from libnagato.Object import NagatoObject
from libmoeclock.Object import NagatoObject


class NagatoSub(Gtk.Menu, NagatoObject):

    TITLE = ""

    def _on_map(self, widget):
        pass

    def _initialize_root_menu_item(self):
        Gtk.Menu.__init__(self)
        self._root_menu_item = Gtk.MenuItem(self.TITLE)
        self._root_menu_item.set_submenu(self)
        self._root_menu_item.connect("map", self._on_map)
        self._parent.append(self._root_menu_item)

    def _initialize_child_menus(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._initialize_root_menu_item()
        self._initialize_child_menus()
