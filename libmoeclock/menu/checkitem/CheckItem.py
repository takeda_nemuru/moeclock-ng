
from gi.repository import Gtk
from libmoeclock.Object import NagatoObject


class NagatoCheckItem(Gtk.CheckMenuItem, NagatoObject):

    RADIO = False

    def _on_map(self, menu_item):
        pass

    def _on_activate(self, *args):
        pass

    def _on_initialize(self, user_data):
        raise NotImplementedError

    def __init__(self, parent, user_data):
        self._parent = parent
        Gtk.CheckMenuItem.__init__(self)
        self.set_draw_as_radio(self.RADIO)
        self.connect("map", self._on_map)
        self._signal_id = self.connect("activate", self._on_activate)
        self._parent.append(self)
        self._on_initialize(user_data)
