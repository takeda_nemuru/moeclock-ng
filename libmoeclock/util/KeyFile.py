
from gi.repository import GLib

CONFIG_NAMES = [GLib.get_user_config_dir(), "moeclock", "application.config"]
CONFIG_FILE_PATH = GLib.build_filenamev(CONFIG_NAMES)


class HaruhiKeyFile:

    def get_value(self, user_data):
        yuki_value = self._key_file.get_value(*user_data)
        try:
            return eval(yuki_value)
        except:
            return yuki_value

    def set_value(self, user_data):
        yuki_group, yuki_key, yuki_value = user_data
        self._key_file.set_value(yuki_group, yuki_key, str(yuki_value))

    def save(self):
        self._key_file.save_to_file(CONFIG_FILE_PATH)

    def __init__(self):
        self._key_file = GLib.KeyFile.new()
        self._key_file.load_from_file(CONFIG_FILE_PATH, GLib.KeyFileFlags.NONE)
