
from gi.repository import GLib
from gi.repository import GstPlayer
from libmoeclock.Object import NagatoObject


class NagatoSoundNotification(NagatoObject):

    def notify(self):
        yuki_path = self._enquiry("YUKI.N > config", ("sound", "path"))
        yuki_uri = GLib.filename_to_uri(yuki_path, None)
        self._player.set_uri(yuki_uri)
        self._player.play()

    def __init__(self, parent):
        self._parent = parent
        self._player = GstPlayer.Player.new(None, None)
        self._player.set_volume(0.5)
