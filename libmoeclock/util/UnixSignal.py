
import signal
from gi.repository import Gtk
from gi.repository import GLib

PRIORITY = GLib.PRIORITY_DEFAULT
SIGNAL = signal.SIGINT


class HaruhiUnixSignal:

    def _on_catch_signal(self):
        Gtk.main_quit()

    def __init__(self):
        GLib.unix_signal_add(PRIORITY, SIGNAL, self._on_catch_signal)
