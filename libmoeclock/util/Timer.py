
from gi.repository import GLib
from libmoeclock.Object import NagatoObject
from libmoeclock.util.SoundNotification import NagatoSoundNotification


class NagatoTimer(NagatoObject):

    def _on_time_out(self):
        yuki_date_time = GLib.DateTime.new_now_local()
        if yuki_date_time.get_second() % 10 == 0:
            self._sound_notification.notify()
            self._raise("YUKI.N > change image")
        self._raise("YUKI.N > queue draw")
        return True

    def __init__(self, parent):
        self._parent = parent
        self._sound_notification = NagatoSoundNotification(self)
        GLib.timeout_add_seconds(1, self._on_time_out)
