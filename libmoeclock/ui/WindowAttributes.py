
from gi.repository import GdkPixbuf
from libmoeclock.Object import NagatoObject


class NagatoWindowAttributes(NagatoObject):

    def save(self):
        yuki_main_window = self._enquiry("YUKI.N > main window")
        yuki_x, yuki_y = yuki_main_window.get_position()
        self._raise("YUKI.N > config", ("window", "x", yuki_x))
        self._raise("YUKI.N > config", ("window", "y", yuki_y))
        yuki_width, yuki_height = yuki_main_window.get_size()
        self._raise("YUKI.N > config", ("window", "width", yuki_width))
        self._raise("YUKI.N > config", ("window", "height", yuki_height))

    def __init__(self, parent):
        self._parent = parent
        yuki_path = self._enquiry("YUKI.N > resource", "application.png")
        yuki_pixbuf = GdkPixbuf.Pixbuf.new_from_file(yuki_path)
        yuki_main_window = self._enquiry("YUKI.N > main window")
        yuki_main_window.set_icon(yuki_pixbuf)
        yuki_x = self._enquiry("YUKI.N > config", ("window", "x"))
        yuki_y = self._enquiry("YUKI.N > config", ("window", "y"))
        yuki_main_window.move(yuki_x, yuki_y)
        yuki_width = self._enquiry("YUKI.N > config", ("window", "width"))
        yuki_height = self._enquiry("YUKI.N > config", ("window", "height"))
        yuki_main_window.set_default_size(yuki_width, yuki_height)
