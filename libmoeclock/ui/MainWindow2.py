
from gi.repository import Gtk
from libmoeclock.Object import NagatoObject
from libmoeclock.ui.WindowAttributes import NagatoWindowAttributes
from libmoeclock.ui.WindowAspect2 import NagatoWindowAspect


class NagatoMainWindow2(Gtk.Window, NagatoObject):

    def _yuki_n_add_to_container(self, widget):
        self.add(widget)

    def _inform_window_size(self):
        return self.get_size()

    def _inform_main_window(self):
        return self

    def _on_close_window(self, window, event, user_data=None):
        self._attributes.save()
        Gtk.main_quit()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Window.__init__(self)
        self._attributes = NagatoWindowAttributes(self)
        NagatoWindowAspect(self)
        self.connect("delete-event", self._on_close_window)
        self.show_all()
        Gtk.main()
