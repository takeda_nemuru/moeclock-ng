
from gi.repository import Gtk
from libmoeclock.Object import NagatoObject
from libmoeclock.wallpaper.Builder import NagatoBuilder
from libmoeclock.util.Timer import NagatoTimer
from libmoeclock.menu.Context import NagatoContext


class NagatoDrawingArea(Gtk.DrawingArea, NagatoObject):

    def _on_draw(self, drawing_area, cairo_context):
        yuki_surface = self._builder.build_()
        cairo_context.set_source_surface(yuki_surface, 0, 0)
        cairo_context.paint()

    def _yuki_n_queue_draw(self):
        self.queue_draw()

    def __init__(self, parent):
        self._parent = parent
        Gtk.DrawingArea.__init__(self)
        self.set_hexpand(True)
        self.set_vexpand(True)
        self._builder = NagatoBuilder(self)
        self._raise("YUKI.N > add to container", self)
        self.connect("draw", self._on_draw)
        NagatoTimer(self)
        NagatoContext(self)
