
from gi.repository import GLib

# FORMAT = "%B%e日\n%A\n<span size='x-large'><b>%R</b></span>\nだよ"
FORMAT = "%B%e日\n%A\n<span size='x-large'><b>%T</b></span>\nだよ"


def get_markup():
    yuki_date_time = GLib.DateTime.new_now_local()
    return yuki_date_time.format(FORMAT)
