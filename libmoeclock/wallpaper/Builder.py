
import cairo
from gi.repository import Gdk
from libmoeclock.Object import NagatoObject
from libmoeclock.wallpaper.ScaledPixbuf import NagatoScaledPixbuf
from libmoeclock.wallpaper.Frame import NagatoFrame
from libmoeclock.wallpaper.Markup import NagatoMarkup


class NagatoBuilder(NagatoObject):

    def build_(self):
        yuki_pixbuf = self._scaled_pixbuf.build_()
        yuki_data = yuki_pixbuf, 1, None
        yuki_surface = Gdk.cairo_surface_create_from_pixbuf(*yuki_data)
        yuki_cairo_context = cairo.Context(yuki_surface)
        self._frame.paint(yuki_cairo_context)
        self._markup.paint(yuki_cairo_context)
        return yuki_surface

    def __init__(self, parent):
        self._parent = parent
        self._scaled_pixbuf = NagatoScaledPixbuf(self)
        self._frame = NagatoFrame(self)
        self._markup = NagatoMarkup(self)
