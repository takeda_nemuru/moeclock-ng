
from gi.repository import GLib
from gi.repository import GdkPixbuf
from libmoeclock.Object import NagatoObject


class NagatoThemed(NagatoObject):

    def _get_pixbuf_from_name(self, name):
        yuki_skin = self._enquiry("YUKI.N > config", ("clock", "skin"))
        yuki_directory = self._enquiry("YUKI.N > resource", "skin")
        yuki_path = GLib.build_filenamev([yuki_directory, yuki_skin, name])
        return GdkPixbuf.Pixbuf.new_from_file(yuki_path)

    def __init__(self, parent):
        self._parent = parent
