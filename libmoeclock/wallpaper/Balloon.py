
from gi.repository import GdkPixbuf
from libmoeclock.wallpaper.Themed import NagatoThemed


class NagatoBalloon(NagatoThemed):

    def composit(self, base_pixbuf):
        yuki_balloon = self._get_pixbuf_from_name("annotation.png")
        yuki_balloon.composite(
            base_pixbuf,
            0,
            0,
            base_pixbuf.get_width(),
            base_pixbuf.get_height(),
            base_pixbuf.get_width()-yuki_balloon.get_width(),
            base_pixbuf.get_height()-yuki_balloon.get_height(),
            1,
            1,
            GdkPixbuf.InterpType.BILINEAR,
            255
            )
