
from gi.repository import Gtk
from gi.repository import GLib

BUTTONS = ("Cancel", Gtk.ResponseType.CANCEL, "Select", Gtk.ResponseType.OK)


class NagatoSelectDirectory(Gtk.FileChooserDialog):

    @classmethod
    def call(self, current_folder=GLib.get_home_dir()):
        yuki_dialog = NagatoSelectDirectory()
        yuki_dialog.set_current_folder(current_folder)
        yuki_response = yuki_dialog.run()
        yuki_path = yuki_dialog.get_current_folder()
        yuki_dialog.destroy()
        if yuki_response == Gtk.ResponseType.OK:
            return yuki_path
        return None

    def __init__(self):
        Gtk.FileChooserDialog.__init__(
            self,
            "Select Directory",
            Gtk.Window(title="Select Directory"),
            Gtk.FileChooserAction.SELECT_FOLDER,
            BUTTONS
            )
