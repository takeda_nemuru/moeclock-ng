
from libmoeclock.Object import NagatoObject
from libmoeclock.util.KeyFile import HaruhiKeyFile
from libmoeclock.dataovermind.ImagePathsLayer import NagatoImagePathsLayer


class NagatoConfigLayer(NagatoObject):

    def _inform_config(self, user_data):
        return self._key_file.get_value(user_data)

    def _yuki_n_config(self, user_data):
        self._key_file.set_value(user_data)

    def _set_default(self):
        if self._key_file.get_value(("sound", "path")) != "":
            return
        yuki_path = self._enquiry("YUKI.N > resource", "sound/sound.wav")
        self._key_file.set_value(("sound", "path", yuki_path))

    def __init__(self, parent):
        self._parent = parent
        self._key_file = HaruhiKeyFile()
        self._set_default()
        NagatoImagePathsLayer(self)
        self._key_file.save()
