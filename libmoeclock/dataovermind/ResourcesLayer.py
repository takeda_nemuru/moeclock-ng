
from gi.repository import GLib
from libmoeclock.Object import NagatoObject
from libmoeclock.dataovermind.LocalFilesLayer import NagatoLocalFilesLayer


class NagatoResourcesLayer(NagatoObject):

    def _get_file_path(self, name):
        yuki_library_directory = self._enquiry("YUKI.N > library directory")
        yuki_names = [yuki_library_directory, "resources", name]
        return GLib.build_filenamev(yuki_names)

    def _inform_resource(self, name):
        return self._get_file_path(name)

    def __init__(self, parent):
        self._parent = parent
        NagatoLocalFilesLayer(self)
