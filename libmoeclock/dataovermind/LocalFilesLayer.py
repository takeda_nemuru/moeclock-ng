
from gi.repository import Gio
from gi.repository import GLib
from libmoeclock.Object import NagatoObject
from libmoeclock.dataovermind.ConfigLayer import NagatoConfigLayer


class NagatoLocalFilesLayer(NagatoObject):

    def _ensure(self, source_path, target_path):
        if GLib.file_test(target_path, GLib.FileTest.EXISTS):
            return
        yuki_source_file = Gio.File.new_for_path(source_path)
        yuki_target_file = Gio.File.new_for_path(target_path)
        yuki_parent_file = yuki_target_file.get_parent()
        yuki_parent_file.make_directory_with_parents(None)
        yuki_source_file.copy(yuki_target_file, Gio.FileCopyFlags.NONE)

    def _ensure_local_file(self, name):
        yuki_target_names = [GLib.get_user_config_dir(), "moeclock", name]
        yuki_target_path = GLib.build_filenamev(yuki_target_names)
        yuki_source_path = self._enquiry("YUKI.N > resource", name)
        self._ensure(yuki_source_path, yuki_target_path)

    def __init__(self, parent):
        self._parent = parent
        self._ensure_local_file("application.config")
        NagatoConfigLayer(self)
